// console.log("Hello world")
//
fetch("https://jsonplaceholder.typicode.com/todos", {
	method : "GET",
})
.then((response) => response.json())
.then((json) => json.map( (json) => {
	let titles = []
	titles += json.title
	return titles;
}))
.then((titles) => console.log(titles));



//
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())

.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));



//
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers:{
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Created To Do List Item",
		userId: 1,
	}) 
})
.then(response => response.json())
.then((json) => console.log(json));



//
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers:{
		"Content-Type" : "application/json"
	},
	// #9 changing data structure
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structur",
		status: "Pending" ,
		title: "Updated To Do List Item",
		UserId: 1
	}) 
})
.then(response => response.json())
.then((json) => console.log(json));



//
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers:{
		"Content-Type" : "application/json"
	},
	
	body: JSON.stringify({
			dateCompleted: "07/09/21",
			status: "Complete" ,
			title: "delectus aut autem",
			UserId: 1
		}) 
})
.then(response => response.json())
.then((json) => console.log(json));



//
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"});



